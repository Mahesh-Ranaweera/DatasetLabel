package endpoints

/**
*Handle page rendering routing
 */

import (
	"fmt"
	"html/template"
	"net/http"
)

type Todo struct {
	Title string
	Done  bool
}

type TodoPageData struct {
	PageTitle string
	Todos     []Todo
}

func Index(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.New("home.html").ParseGlob("./view/*.html"))
	// data := TodoPageData{
	// 	PageTitle: "My TODO list",
	// 	Todos: []Todo{
	// 		{Title: "Task 1", Done: false},
	// 		{Title: "Task 2", Done: true},
	// 		{Title: "Task 3", Done: true},
	// 	},
	// }
	tmpl.Execute(w, nil)
}

func Signin(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.New("login.html").ParseGlob("./view/*.html"))
	tmpl.Execute(w, nil)
}

func Signup(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Hello World")
}

func Dashboard(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.New("dashboard.html").ParseGlob("./view/*.html"))
	tmpl.Execute(w, nil)
}
