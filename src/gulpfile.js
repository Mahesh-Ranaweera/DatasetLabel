'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

sass.compiler = require('node-sass');

gulp.task('sass', function () {
  return gulp.src('./js_src/style/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
    	Browserslist: ['last 2 versions'],
    	cascade: false
    }))
    .pipe(cleanCSS())
    .pipe(gulp.dest('./static/css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./js_src/style/*.scss', gulp.series('sass'));
});