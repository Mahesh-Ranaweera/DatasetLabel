import { Dashboard } from "./modules/dashboard";

declare var render_page:any;

class Backend {
    constructor() {
        console.log('Backend Loaded::' + render_page);

        switch( render_page ) {
            case "dashboard":
                new Dashboard();
                break;
            default:
                console.log('Page Not Defined');
        }
     }
}

new Backend();