import { UIHandler } from "./uihandler";

export class Dashboard extends UIHandler {
    constructor() {
        super('dashboard');

        this.render_page();
    }

    private async render_page() {
        console.log('rendering dashboard page');

        this.set_header(`
            <div class='app--icon'>Home</div>
            <div class='app--icon'>Button</div>
            <div class='app--icon'>Button</div>
        `);
    }
}