var jQuery =  require('jquery');
import * as _ from "underscore";

export class UIHandler {
    private pageId:string;
    private static header:string = "app--header";
    private static body:string   = "app--body";
    private static footer:string = "app--footer";
    
    constructor( pageId:string ) {
        this.pageId = pageId;
    }

    public clear_header() {
        jQuery( `#${UIHandler.header}` ).html('');
    }

    public clear_footer() {
        jQuery(`#${UIHandler.footer}`).html('');
    }

    public clear_body() {
        jQuery(`#${UIHandler.body}`).html('');
    }

    public set_header( content:string ) {
        jQuery( `#${UIHandler.header}` ).html(content);
    }

    public set_body( content:string ) {
        jQuery(`#${UIHandler.body}`).html(content);
    }

    public set_footer( content:string ) {
        jQuery(`#${UIHandler.footer}`).html(content);
    }
}