package main

import (
	"fmt"
	"net/http"

	endpoints "./backend/endpoints"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)
	//apipath := router.PathPrefix("/apipath/v1")
	//apipath.Methods("GET").Path("/test").HandlerFunc(dashboard)

	router.Methods("GET").Path("/").HandlerFunc(endpoints.Index)
	router.Methods("GET").Path("/login").HandlerFunc(endpoints.Signin)

	// admin side
	router.Methods("GET").Path("/dashboard").HandlerFunc(endpoints.Dashboard)

	// set static folder
	router.PathPrefix("/").Handler(http.FileServer(http.Dir("./static")))

	fmt.Println("server on http://localhost:8000")
	http.ListenAndServe(":8000", router)
}
