/**
 * WebPack Config
 */

var path = require("path");
var webpack = require("webpack");

module.exports = {
    entry: {
        frontend: './js_src/script/frontend.ts',
        backend: './js_src/script/backend.ts'
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "sass-loader" }
                ]
            },
            {
                test: /\.svg$/,
                loader: 'svg-inline-loader'
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.json'],
    },
    node: {
        fs: 'empty'
    },
    output: {
        path: path.resolve(__dirname, 'static/js'),
        filename: "[name].js"
    },
    optimization: {
        //minify settings
        minimize: true,
    },
    // change the mod to production : when export : to dev : development
    mode: "development",
    //live compile
    watch: true
};